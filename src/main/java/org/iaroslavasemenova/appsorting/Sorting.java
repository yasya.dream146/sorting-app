package org.iaroslavasemenova.appsorting;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Arrays;

public class Sorting {

    private static final Logger logger
            = LoggerFactory.getLogger(Sorting.class);

    public String sort(String[] array) {
        validate(array);
        logger.info("Sorting is started");
            int[] numbers = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                numbers[i] = Integer.parseInt(array[i]);
            }
            Arrays.sort(numbers);
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < numbers.length; i++) {
                 str.append(numbers[i]).append(" ");
            }
            return str.toString().trim();
        }

    private void validate(String[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Null value entered");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Input array is empty");
        } else if (array.length > 10) {
            throw new IllegalArgumentException("Input array has more than 10 elements");
        }
        for (int i = 0; i < array.length; i++) {
            try {
                Integer.parseInt(array[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Non integer element: " + array[i]);
            }
        }
    }
}

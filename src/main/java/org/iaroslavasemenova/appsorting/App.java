package org.iaroslavasemenova.appsorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);


    /**
     * Prints integer values sorted in ascending order to standard output.
     *
     * @param array Array to sort
     * @author Iaroslava Semenova
     */
    public static void main(String[] array) {
        LOGGER.info("Application launched");

        Sorting sorting = new Sorting();
        try {
            LOGGER.info(" {}", sorting.sort(array));
        } catch (IllegalArgumentException e) {
            LOGGER.error("Unexpected argument", e);
        }
        LOGGER.info("Application completed");
    }
}

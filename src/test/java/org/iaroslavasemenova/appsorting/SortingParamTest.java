package org.iaroslavasemenova.appsorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingParamTest {
    Sorting sorting = new Sorting();
    private String[] array;
    private String expected;
    public SortingParamTest(String[] array, String expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[] {"3", "4", "2", "1"}, "1 2 3 4"},
                {new String[] {"12", "-4", "3", "5"}, "-4 3 5 12"},
                {new String[] {"235", "13", "2", "1", "21"}, "1 2 13 21 235"},
                {new String[] {"3", "33", "2", "1"}, "1 2 3 33"},
        });
    }
    @Test
    public void testSortedArraysCase() {
        String actual = sorting.sort(array);
        assertEquals(expected, actual);
    }
}
package org.iaroslavasemenova.appsorting;

import org.junit.Test;

public class SortingTest {

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        sorting.sort(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testEmptyCase() {
        sorting.sort(new String[]{});
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreTenArguments() {
        sorting.sort(new String[11]);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testCharArguments() {
        sorting.sort(new String[]{"f"});
    }

    @Test (expected = IllegalArgumentException.class)
    public void testNotIntegerArguments() {
        sorting.sort(new String[]{"1.2"});
    }
}
